package com.example.todomoj.activities;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.example.todomoj.AboutDijalog;
import com.example.todomoj.DatabaseHelper;
import com.example.todomoj.R;
import com.example.todomoj.adapteri.RVAdapter;
import com.example.todomoj.model.Grupa;
import com.j256.ormlite.android.apptools.OpenHelperManager;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity implements RVAdapter.OnRVClickedListener {

    private Toolbar toolbar;
    private SharedPreferences sharedPreferences;
    private List<String> drawerItems;
    private ActionBarDrawerToggle drawerToggle;
    private AlertDialog aboutDijalog;

    private DatabaseHelper databaseHelper;
    public static String TOAST_SETTINGS = "toast_settings_cb";
    public static String FILTER_SETTINGS = "prikaz_settings_cb";

    private List<Grupa> listaGrupa;
    private RecyclerView recyclerView;
    private RVAdapter rvAdapter;

    private Date currentTime;
    private EditText etNaziv;
    private EditText etOznaka;
    private Button btnCancel;
    private Button btnSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupToolbar();
        fillDrawerWithItems();
        setupDrawer();
        showList();


        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
    }

    private void setupToolbar() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_action_home);
            actionBar.show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add:
                dodajGrupu();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void dodajGrupu(){

        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dodaj_grupu);
        /* dialog.setCancelable(false);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams(); // dijalog da bude 90% screena
        lp.copyFrom(dialog.getWindow().getAttributes()); //
        lp.width = WindowManager.LayoutParams.MATCH_PARENT; //
        lp.height = WindowManager.LayoutParams.MATCH_PARENT; //
        dialog.getWindow().setAttributes(lp);*/ //

        etNaziv = dialog.findViewById(R.id.etNaziv);
        etOznaka = dialog.findViewById(R.id.etOznaka);

        btnSave = dialog.findViewById(R.id.btnSave);
        btnCancel = dialog.findViewById(R.id.btnCancel);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // ovo nam je Nikola pokazivao na predavanju
                DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss"); //u ovom foramtu ce se ispisivati datum
                currentTime = Calendar.getInstance().getTime(); //vraca tremutno vreme za upis u bazu
                dateFormat.format(currentTime); //formatira i vraca string

                Grupa grupa = new Grupa();

                grupa.setNaziv(etNaziv.getText().toString());
                grupa.setOznaka(etOznaka.getText().toString());
                grupa.setDatum(dateFormat.format(currentTime));
                listaGrupa.add(grupa);

                try {
                    getDatabaseHelper().getGrupaDao().create(grupa);
                    showToast("Grupa dodata");
                    refresh();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                dialog.dismiss();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showToast("Niste dodali novu grupu");
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void fillDrawerWithItems() {
        drawerItems = new ArrayList<>();
        drawerItems.add("Sve grupe");
        drawerItems.add("Podesavanja");
        drawerItems.add("O aplikaciji");
    }

    private void setupDrawer() {
        final LinearLayout linearZaDrawer = findViewById(R.id.linear_za_drawerList);
        final DrawerLayout masterDrawerLayout = findViewById(R.id.masterDrawerLayout);
        ListView lvDrawer = findViewById(R.id.lvDrawer);
        lvDrawer.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, drawerItems));
        lvDrawer.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String naslov = "nepoznato";
                switch (position) {
                    case 0:
                        naslov = "Sve grupe";
                        showList();
                        break;
                    case 1:
                        naslov = "Podesavanja";
                        showSettings();
                        break;
                    case 2:
                        naslov = "O aplikaciji";
                        showDijalog();
                }
                setTitle(naslov);
                masterDrawerLayout.closeDrawer(linearZaDrawer);
            }
        });
        drawerToggle = new ActionBarDrawerToggle(this, masterDrawerLayout, toolbar, R.string.app_name, R.string.app_name) {
            public void onDrawerClosed(View view) {
                invalidateOptionsMenu();
            }

            public void onDrawerOpened(View view) {
                invalidateOptionsMenu();
            }
        };
        masterDrawerLayout.closeDrawer(linearZaDrawer);
    }

    public void showList() {

        try {
            listaGrupa = getDatabaseHelper().getGrupaDao().queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if (listaGrupa == null) {
            listaGrupa = new ArrayList<>();
        }

        recyclerView = findViewById(R.id.rvListaAtrakcija);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setReverseLayout(true);
        layoutManager.setStackFromEnd(true);  //ubacuje na pocetak prvi element a ne na dno
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(new RVAdapter(listaGrupa, this));
    }

    private void refresh() {
        if (recyclerView != null) {
            rvAdapter = (RVAdapter) recyclerView.getAdapter();
            if (rvAdapter != null) {
                try {
                    listaGrupa = getDatabaseHelper().getGrupaDao().queryForAll();
                    rvAdapter.notifyDataSetChanged();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void showSettings() {
        Intent intent = new Intent(this, PrefsActivity.class);
        startActivity(intent);
    }

    private void showDijalog() {
        if (aboutDijalog == null) {
            aboutDijalog = new AboutDijalog(this).prepareDialog();
        } else {
            if (aboutDijalog.isShowing()) {
                aboutDijalog.dismiss();
            }
        }
        aboutDijalog.show();
    }

    public DatabaseHelper getDatabaseHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // nakon rada sa bazo podataka potrebno je obavezno
        //osloboditi resurse!
        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        refresh();
    }

    private void showToast(String message) {
        boolean toast = sharedPreferences.getBoolean(TOAST_SETTINGS, false);
        if (toast) Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRVClicked(Grupa grupa) {
        Intent intent = new Intent(this, DetailsActivity.class);
        intent.putExtra("grupa_id", grupa.getId());
        startActivity(intent);
    }
}
