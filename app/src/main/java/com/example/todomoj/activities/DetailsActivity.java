package com.example.todomoj.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.todomoj.AboutDijalog;
import com.example.todomoj.DatabaseHelper;
import com.example.todomoj.R;
import com.example.todomoj.adapteri.RVAdapter;
import com.example.todomoj.adapteri.RVAdapterTODO;
import com.example.todomoj.model.Grupa;
import com.example.todomoj.model.TODOZadatak;
import com.j256.ormlite.android.apptools.OpenHelperManager;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DetailsActivity extends AppCompatActivity implements RVAdapterTODO.OnRVTODOClickedListener {

    private DatabaseHelper databaseHelper;
    private Grupa grupa;
    private Toolbar toolbar;
    private SharedPreferences sharedPreferences;
    private List<String> drawerItems;
    private ActionBarDrawerToggle drawerToggle;

    private AlertDialog aboutDijalog;
    private AlertDialog dijalogBrisanje;

    public static String TOAST_SETTINGS = "toast_settings_cb";

    private List<TODOZadatak> listaTODOzadataka;
    private RecyclerView rvTODO;
    private ArrayAdapter adapterTODO;
    private TODOZadatak todo;

    private ListView lvOznakaDetalji;
    private List<Grupa> listaGrupa;
    private RecyclerView recyclerView;
    private RVAdapter rvAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        setupToolbar();
        fillDrawerWithItems();
        setupDrawer();
        showDetails(getIntent());

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
    }

    private void setupToolbar() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_action_home);
            actionBar.show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_details, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add:
                dodajTODOzadatak();
                break;
            case R.id.action_edit:
                izmeniGrupu();
                break;
            case R.id.action_delete:
                showDijalogBrisanje();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void fillDrawerWithItems() {
        drawerItems = new ArrayList<>();
        drawerItems.add("Sve grupe");
        drawerItems.add("Podesavanja");
        drawerItems.add("O aplikaciji");
    }

    private void setupDrawer() {
        final LinearLayout linearZaDrawer = findViewById(R.id.linear_za_drawerList);
        final DrawerLayout masterDrawerLayout = findViewById(R.id.masterDrawerLayout);
        ListView lvDrawer = findViewById(R.id.lvDrawer);
        lvDrawer.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, drawerItems));
        lvDrawer.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String naslov = "nepoznato";
                switch (position) {
                    case 0:
                        naslov = "Sve grupe";
                        break;
                    case 1:
                        naslov = "Podesavanja";
                        showSettings();
                        break;
                    case 2:
                        naslov = "O aplikaciji";
                        showDijalog();
                }
                setTitle(naslov);
                masterDrawerLayout.closeDrawer(linearZaDrawer);
            }
        });
        drawerToggle = new ActionBarDrawerToggle(this, masterDrawerLayout, toolbar, R.string.app_name, R.string.app_name) {
            public void onDrawerClosed(View view) {
                invalidateOptionsMenu();
            }

            public void onDrawerOpened(View view) {
                invalidateOptionsMenu();
            }
        };
        masterDrawerLayout.closeDrawer(linearZaDrawer);
    }

    private void showSettings() {
        Intent intent = new Intent(this, PrefsActivity.class);
        startActivity(intent);
    }

    private void showDijalog() {
        if (aboutDijalog == null) {
            aboutDijalog = new AboutDijalog(this).prepareDialog();
        } else {
            if (aboutDijalog.isShowing()) {
                aboutDijalog.dismiss();
            }
        }
        aboutDijalog.show();
    }

    public void showDetails(final Intent intent) {

        TextView etNazivDetalji = findViewById(R.id.etNazivDetalji);
        TextView tvDatumDetalji = findViewById(R.id.tvDatumDetalji);
        lvOznakaDetalji = findViewById(R.id.lvOznakaDetalji);

        if (listaTODOzadataka == null) {
            listaTODOzadataka = new ArrayList<>();
        }
        rvTODO = findViewById(R.id.rvTODO);

        final int grupaID = getIntent().getExtras().getInt("grupa_id");

        try {
            grupa = getDatabaseHelper().getGrupaDao().queryForId(grupaID);
            listaTODOzadataka = getDatabaseHelper().getTodoDao().queryForEq("grupa_id", grupaID);

            LinearLayoutManager layoutManager = new LinearLayoutManager(this);
            layoutManager.setReverseLayout(true);
            layoutManager.setStackFromEnd(true);
            rvTODO.setLayoutManager(layoutManager);
            rvTODO.setAdapter(new RVAdapterTODO(listaTODOzadataka, this));

            String oznake = grupa.getOznaka();
            String [] oznakeZaListu = oznake.split(", ");
            ArrayAdapter adapterOznake = new ArrayAdapter(this,
                    android.R.layout.simple_list_item_1, oznakeZaListu);
            lvOznakaDetalji.setAdapter(adapterOznake);
            refresh();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        etNazivDetalji.setText(grupa.getNaziv());
        tvDatumDetalji.setText("Zadatak kreiran: " + grupa.getDatum());
    }

    public class BrisanjeDijalog extends AlertDialog.Builder {

        public BrisanjeDijalog(@NonNull Context context) {
            super(context);
            setTitle("Obrisi grupu");
            setMessage("Zelite li da obrisete ovu grupu?");
            setPositiveButton("U redu", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    obrisiGrupu();
                }
            });
            setNegativeButton("Otkazi", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
        }

        public AlertDialog prepareDialog() {
            AlertDialog alertDialog = create();
            alertDialog.setCanceledOnTouchOutside(false);
            return alertDialog;
        }
    }

    private void showDijalogBrisanje() {
        if (dijalogBrisanje != null) {
            dijalogBrisanje = new BrisanjeDijalog(this).prepareDialog();
        } else {
            if (dijalogBrisanje.isShowing()) {
                dijalogBrisanje.dismiss();
            }
        }
        dijalogBrisanje.show();
    }

    private void obrisiGrupu() {
        if (grupa != null) {
            try {
                getDatabaseHelper().getGrupaDao().delete(grupa);
                refresh();
                showToast("Grupa je obrisana");
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        onBackPressed();
    }

    private void izmeniGrupu() {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.izmeni_grupu);

        final EditText etNazivIzmena = dialog.findViewById(R.id.etNazivIzmena);

        Button btnCancel = dialog.findViewById(R.id.btnCancel);
        Button btnSave = dialog.findViewById(R.id.btnSave);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (grupa != null) {
                    grupa.setNaziv(etNazivIzmena.getText().toString());
                    try {
                        getDatabaseHelper().getGrupaDao().update(grupa);
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    dialog.dismiss();
                }
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }


    private void dodajTODOzadatak() {

        int grupaID = grupa.getId();
        Intent intent = new Intent(this, DodajTODOZadatak.class);
        intent.putExtra("grupa_id",grupaID);
        startActivity(intent);
    }

    private void showToast(String message) {
        boolean toast = sharedPreferences.getBoolean(TOAST_SETTINGS, false);
        if (toast) Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    public void refresh() {
        if (rvTODO != null) {
            adapterTODO = new ArrayAdapter(DetailsActivity.this, android.R.layout.simple_list_item_1, listaTODOzadataka);
            if (adapterTODO != null) {
                try {
                    listaTODOzadataka = getDatabaseHelper().getTodoDao().queryForAll();
                    adapterTODO.notifyDataSetChanged();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public DatabaseHelper getDatabaseHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // nakon rada sa bazom podataka potrebno je obavezno
        //osloboditi resurse!
        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }

    @Override
    public void onRVTODOClicked(TODOZadatak todoZadatak) {
        Intent intent1 = new Intent(DetailsActivity.this, DetaljiTODOActivity.class);
        intent1.putExtra("todoID", todoZadatak.getId());
        startActivity(intent1);
    }
}
