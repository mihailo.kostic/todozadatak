package com.example.todomoj.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;

import com.example.todomoj.DatabaseHelper;
import com.example.todomoj.R;
import com.example.todomoj.model.Grupa;
import com.example.todomoj.model.TODOZadatak;
import com.j256.ormlite.android.apptools.OpenHelperManager;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DodajTODOZadatak extends AppCompatActivity {

    private EditText etNazivTODO;
    private EditText etOpisTODO;
    private EditText etPrioritetTODO;
    private EditText etDatumZavrsetkaTODO;
    private EditText etStatusTODO;
    Button btnCancel;
    Button btnAdd;

    private DatabaseHelper databaseHelper;

    private Date currentTime;
    private Grupa grupa;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dodaj_todozadatak);

        etNazivTODO = findViewById(R.id.etNazivTODO);
        etOpisTODO = findViewById(R.id.etOpisTODO);
        etPrioritetTODO = findViewById(R.id.etPrioritetTODO);
        etDatumZavrsetkaTODO = findViewById(R.id.etDatumZavrsetkaTODO);
        etStatusTODO = findViewById(R.id.etStatusTODO);

        btnAdd = findViewById(R.id.btnAdd);
        btnCancel = findViewById(R.id.btnCancel);

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validacijaTODO(etNazivTODO) && validacijaTODO(etPrioritetTODO) &&
                        validacijaTODO(etDatumZavrsetkaTODO) && validacijaTODO(etStatusTODO)) {
                    addTODOZadatak();
                }
            }
        });
    }

    private void addTODOZadatak() {

        int grupaID = getIntent().getExtras().getInt("grupa_id");

        try {
            grupa = getDatabaseHelper().getGrupaDao().queryForId(grupaID);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss"); //u ovom foramtu ce se ispisivati datum
        currentTime = Calendar.getInstance().getTime(); //vraca tremutno vreme za upis u bazu
        dateFormat.format(currentTime); //formatira i vraca string

        Calendar now = Calendar.getInstance();
        now.add(Calendar.MINUTE, Integer.parseInt(etDatumZavrsetkaTODO.getText().toString()));

        TODOZadatak todoZadatak = new TODOZadatak();

        todoZadatak.setNaziv(etNazivTODO.getText().toString());
        todoZadatak.setOpis(etOpisTODO.getText().toString());
        todoZadatak.setPrioritet(etPrioritetTODO.getText().toString());
        todoZadatak.setStatus(etStatusTODO.getText().toString());

        todoZadatak.setDatumKreiranja(dateFormat.format(currentTime));
        todoZadatak.setDatumZavrsetka(dateFormat.format(now.getTime()));
        todoZadatak.setGrupa(grupa);

        try {
            getDatabaseHelper().getTodoDao().create(todoZadatak);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        Intent intent = new Intent();
        intent.putExtra("grupa_id", grupaID);
        startActivity(intent);
    }

    public boolean validacijaTODO(EditText editText) {

        String editTitle = editText.getText().toString().trim();

        if (editTitle.isEmpty()) {
            editText.setError("Polje ne sme biti prazno");
            return false;
        } else {
            editText.setError(null);
            return true;
        }
    }

    public DatabaseHelper getDatabaseHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }

}
