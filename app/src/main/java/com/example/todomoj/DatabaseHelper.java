package com.example.todomoj;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.example.todomoj.model.Grupa;
import com.example.todomoj.model.TODOZadatak;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    public static final String DATABASE_NAME = "TODOZadaci.db";
    public static final int DATABASE_VERSION = 1;

    public DatabaseHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    private Dao<Grupa, Integer> grupaDao = null;
    private Dao<TODOZadatak, Integer> todoDao = null;

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {

        try {
            TableUtils.createTable(connectionSource, Grupa.class);
            TableUtils.createTable(connectionSource, TODOZadatak.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {

        try {
            TableUtils.dropTable(connectionSource, Grupa.class, true);
            TableUtils.dropTable(connectionSource, TODOZadatak.class, true);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Dao<Grupa, Integer> getGrupaDao() throws SQLException {
        if (grupaDao == null) {
            grupaDao = getDao(Grupa.class);
        }
        return grupaDao;
    }

    public Dao<TODOZadatak, Integer> getTodoDao() throws SQLException{
        if (todoDao == null) {
            todoDao = getDao(TODOZadatak.class);
        }
        return todoDao;
    }

    @Override
    public void close() {
        grupaDao = null;
        todoDao = null;
        super.close();
    }
}
